import React from 'react';
import PeterComponent from '../../components/PeterComponents/PeterComponents';
import './peter.css';

function Peter() {
  return <PeterComponent />;
}

export default Peter;
