import './Home.css';
import React from "react";

function Home(){
  return (
    <div>
      <h1>Home</h1>
      <li><a href="../charles">Charles</a></li>
      <li><a href="../peter">Peter</a></li>
      <li><a href="../samantha-tan">Samantha</a></li>
      <li><a href="../ben">Ben Chen</a></li>
      <li><a href="../Yawen">Yawen Zhao</a></li>
    </div>
  );
}

export default Home;