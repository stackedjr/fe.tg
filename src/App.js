// import logo from './logo.svg';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './pages/Home/Home';
import Samantha from './pages/Samantha/Samantha';
import Charless from './pages/Charles/Charles';
import ErrorPage from './pages/ErrorPage/ErrorPage';
import Ben from './pages/Ben/BenPage'
import Peter from './pages/peter/peter';
import Yawen from './pages/Yawen/Yawen';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/ben" exact component={Ben} />
        <Route path="/peter" exact component={Peter} />
        <Route path="/charles" exact component={Charless} />
        <Route path="/samantha" exact component={Samantha} />
        <Route path="/Yawen" exact component={Yawen} />
        <Route component={ErrorPage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
